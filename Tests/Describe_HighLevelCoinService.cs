﻿using System.Collections.Generic;
using BitcoinLib.Responses;
using BitcoinLib.Services.Coins.Cryptocoin;
using CasinoBot;
using FakeItEasy;
using FluentAssertions;
using Xunit;

namespace Tests {
    public sealed class Describe_HighLevelCoinService {
        readonly CoinLibHighLevelCoinService service;
        readonly ICryptocoinService lowLevelService;
        CryptoAccountProvider dummyAccountProvider => A.Dummy<CryptoAccountProvider>();
        CryptoAddressProvider dummyAddressProvider => A.Dummy<CryptoAddressProvider>();

        public Describe_HighLevelCoinService() {
            lowLevelService = A.Fake<ICryptocoinService>();
            var config = new DefaultRpcConfig();
            service = new CoinLibHighLevelCoinService(new CryptoCoinServiceAdapter(config, lowLevelService), config);
        }

        [Fact] public void whenWithdrawing_itFailsIfFirstAddressWrong() {
            A.CallTo(() => lowLevelService.GetAddressesByAccount(A<string>._)).Returns(new List<string>());
            
            service.send(dummyAccountProvider, dummyAddressProvider, 1).validationResult
                .Should().Be(SendCoinsResult.invalidSourceAddress);
        }
        
        [Fact] public void whenWithdrawing_itHandlesNullListOfAddresses() {
            A.CallTo(() => lowLevelService.GetAddressesByAccount(A<string>._)).Returns(null);
            
            service.send(dummyAccountProvider, dummyAddressProvider, 1).validationResult
                .Should().Be(SendCoinsResult.invalidSourceAddress);
        }
        
        [Fact] public void whenWithdrawing_itFailsIfSecondAddressWrong() {
            A.CallTo(() => lowLevelService.GetAddressesByAccount(A<string>._)).Returns(new List<string>(){"asd"});
            A.CallTo(() => lowLevelService.ValidateAddress(A<string>._)).Returns(new ValidateAddressResponse {IsValid = false});
            service.send(dummyAccountProvider, dummyAddressProvider, 1).validationResult
                .Should().Be(SendCoinsResult.invalidDestinationAddress);
        }
        
        [Fact] public void whenWithdrawing_itFailsIfInsufficientBalance() {
            A.CallTo(() => lowLevelService.GetAddressesByAccount(A<string>._)).Returns(new List<string>(){"asd"});
            A.CallTo(() => lowLevelService.ValidateAddress(A<string>._)).Returns(new ValidateAddressResponse {IsValid = true});
            A.CallTo(() => lowLevelService.GetBalance(A<string>._, A<int>._, default)).Returns(0);
            
            service.send(dummyAccountProvider, dummyAddressProvider, 1).validationResult
                .Should().Be(SendCoinsResult.insufficientBalance);
        }
    }
}