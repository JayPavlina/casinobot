﻿using CasinoBot;
using FakeItEasy;
using FluentAssertions;
using Xunit;

namespace Tests {
    public sealed class Describe_UserDataService {
        readonly HighLevelCoinService coinService;
        readonly UserDataService userDataService;

        public Describe_UserDataService() {
            coinService = A.Fake<HighLevelCoinService>();
            userDataService = new UserDataService(coinService);
        }

        [Fact] public void whenGettingAUser_itHasTheCorrectAccountName() {
            var userData = userDataService.getUserData(1);
            userData.accountName.Should().Be("1");
        }
        
        [Fact] public void whenGettingAUser_itHasTheCorrectWalletAddress() {
            A.CallTo(() => coinService.getWalletAddress(A<CryptoAccountProvider>._)).Returns("ADDRESS");
            var userData = userDataService.getUserData(1);
            userData.walletAddress.Should().Be("ADDRESS");
        }
    }
}