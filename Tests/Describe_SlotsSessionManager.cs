﻿/*
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using CasinoBot;
using CasinoBot.Entities;
using FluentAssertions;
using Microsoft.Reactive.Testing;
using Xunit;

namespace Tests {
    public sealed class Describe_SlotsSessionManager {
        readonly SlotsSessionManager slotsSessionManager;
        readonly TestScheduler scheduler;
        IScheduler ischeduler => scheduler;

        public Describe_SlotsSessionManager() {
            scheduler = new TestScheduler();
            var slotsService = new SlotsService(new SlotsConfig(), new DotNetRandomService());
            slotsSessionManager = new SlotsSessionManager(new SlotsConfig(30), scheduler, slotsService);
        }

        [Fact] public void whenStartingASingleSession_itSucceeds() {
            slotsSessionManager.attemptStartSession(1, "bob", null).Should().Be(StartSessionResult.success);
            slotsSessionManager.currentSession.Should().NotBeNull();
        }
        
        [Fact] public void whenStartingASessionWithSameUser_itRefreshes() {
            slotsSessionManager.attemptStartSession(1, "bob", null);
            slotsSessionManager.attemptStartSession(1, "bob", null).Should().Be(StartSessionResult.refresh);
        }

        [Fact] public void whenStartingASession_itFailsIfAnotherExists() {
            slotsSessionManager.attemptStartSession(1, "bob", null);
            slotsSessionManager.attemptStartSession(2, "jill", null).Should().Be(StartSessionResult.busy);
        }
        
        [Fact] public void whenStartingASession_itCachesTheUserId() {
            slotsSessionManager.attemptStartSession(1, "bob", null);
            slotsSessionManager.currentSession.userId.Should().Be(1);
        }
        
        [Fact] public void whenStartingASession_userIdMustNotBeZero() {
            slotsSessionManager.Invoking(x => x.attemptStartSession(0, "", null)).Should().Throw<ArgumentException>();
        }
        

        [Fact] public void afterStartingASession_itEndsAfterTheSetTime() {
            slotsSessionManager.attemptStartSession(1, "bob", null);
            Observable.Timer(TimeSpan.FromSeconds(15), ischeduler)
                .Subscribe(_ => slotsSessionManager.currentSession.Should().NotBeNull());
            Observable.Timer(TimeSpan.FromSeconds(45), ischeduler)
                .Subscribe(_ => slotsSessionManager.currentSession.Should().BeNull());
            scheduler.Start();
        }
    }
}
*/