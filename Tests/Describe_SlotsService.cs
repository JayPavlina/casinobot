﻿using System;
using CasinoBot;
using FakeItEasy;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace Tests {
    public sealed class Describe_SlotsService {
        SlotsService slotsService;
        readonly DefaultSlotsConfig slotsConfig = new DefaultSlotsConfig();

        void initiate() => slotsService = new SlotsService(slotsConfig, new DotNetRandomService());

        [Fact] public void itDistributesWeights() {
//            var populateConfigSystem = new PopulateConfigSystem(A.Dummy<BotConfig>(), slotsConfig);
//            populateConfigSystem.populate();
                slotsConfig.reelItems = new[] {
                    new ReelItem("", new[] {24, 28, 3}, 0),
                    new ReelItem("", new[] {15, 12, 35}, 1),
                    new ReelItem("", new[] {25, 24, 26}, 2),
                };
                initiate();
            
                slotsService.Invoking(x => x.getReelItem(0, 64)).Should().Throw<Exception>();
                slotsService.Invoking(x => x.getReelItem(3, 0)).Should().Throw<Exception>();
                
                test(0, 0, 0);
                test(0, 23, 0);
                test(0, 24, 1);
                test(0, 38, 1);
                test(0, 39, 2);
                test(0, 63, 2);
                
                test(1, 22, 0);
                test(1, 27, 0);
                test(1, 28, 1);
                test(1, 29, 1);
                test(1, 40, 2);
                test(1, 50, 2);
                
                test(2, 1, 0);
                test(2, 3, 1);
                test(2, 37, 1);
                test(2, 38, 2);
                test(2, 63, 2);
            
            void test(int reelId, int weightId, int expectedReelId) => 
                slotsService.getReelItem(reelId, weightId).index.Should().Be(expectedReelId); 
        }
        
        [Theory] 
        [InlineData(0, 0, 0, 1, 0)]
        [InlineData(5, 1, 0, 1, 2)]
        [InlineData(5, 5, 0, 1, 10)]
        [InlineData(5, 5, 5, 1, 1000)]
        [InlineData(5, 5, 5, 2, 2000)]
        [InlineData(6, 6, 6, 1, 5000)]
        [InlineData(3, 3, 3, 3, 300)]
        public void testRewards(int x, int y, int z, int betAmount, int expected) {
            var populateConfigSystem = new PopulateConfigSystem(A.Dummy<BotConfig>(), A.Dummy<RpcConfig>(), slotsConfig);
            populateConfigSystem.populate();
            initiate();
            slotsService.initialize();
            slotsService.getReward(betAmount, (x, y, z)).Should().Be(expected);
        }

//        [Fact] public void youCanBetCoins() {
//            var result = slotsService.bet(1);
//            var possibleRewards = new[] {100, 16, 20, 50, 1, 0};
//            possibleRewards.Should().Contain(result.reward);
//        }
//
//        [Fact] public void bettingCoinsResetsCooldownTimer() {
//            var result = slotsService.bet(1);
//            var possibleRewards = new[] {100, 16, 20, 50, 1, 0};
//            possibleRewards.Should().Contain(result.reward);
//        }
    }
}