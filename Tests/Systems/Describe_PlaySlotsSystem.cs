﻿using CasinoBot;
using FakeItEasy;
using FluentAssertions;
using Xunit;
using Result = CasinoBot.PlaySlotsSystem.ValidationResult;

namespace Tests.Systems {
    public sealed class Describe_PlaySlotsSystem {
        readonly PlaySlotsSystem system;

        public Describe_PlaySlotsSystem() {
            var config = new DefaultSlotsConfig();
            var configProvider = new ConfigProvider(A.Dummy<RpcConfig>(), config);
            var slotsService = new SlotsService(config, new DotNetRandomService());
            system = new PlaySlotsSystem(A.Dummy<EventService>(), slotsService,
                A.Dummy<SendMessageService>(), configProvider, A.Dummy<TreasuryService>(), A.Dummy<UserDataService>());
        }

        [Theory]
        [InlineData(null, Result.wrongFormat)]
        [InlineData(4, Result.invalidBetAmount)]
        [InlineData(0, Result.invalidBetAmount)]
        [InlineData(-1, Result.invalidBetAmount)]
        [InlineData(1, Result.valid)]
        [InlineData(2, Result.valid)]
        [InlineData(3, Result.valid)]
        public void whenValidating_itWorks(int? betAmount, Result result) {
            system.validate(betAmount).Should().Be(result);
        }
    }
}