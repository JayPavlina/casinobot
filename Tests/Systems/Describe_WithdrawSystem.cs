﻿using CasinoBot;
using FakeItEasy;
using FluentAssertions;
using Xunit;

namespace Tests.Systems {
    public sealed class Describe_WithdrawSystem {
        readonly WithdrawSystem system;

        public Describe_WithdrawSystem() {
            var coinService = A.Dummy<HighLevelCoinService>();
            system = new WithdrawSystem(new ReactiveEventService(), coinService, new UserDataService(coinService), A.Dummy<BotConfig>());
        }

        [Theory]
        [InlineData("", "", false)]
        [InlineData(null, "10", false)]
        [InlineData("", "10", false)]
        [InlineData("asdf", "8.1621", true)]
        [InlineData("asdf", WithdrawSystem.allCoins, true)]
        public void validate(string address, string amount, bool expected) {
            system.validateArgs(address, amount).Should().Be(expected);
        }
    }
}