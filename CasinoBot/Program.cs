﻿using System.Reflection;
using System.Threading.Tasks;
using Ninject;

namespace CasinoBot {
    class Program {
        static async Task Main() {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Get<PopulateConfigSystem>().populate();
            kernel.Get<InitializableManager>().initialize();
            kernel.Get<SlotsService>().initialize(); 
            await kernel.Get<InitializeBotSystem>().initialize();
        }
    }
}