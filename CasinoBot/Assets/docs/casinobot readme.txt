Casinobot can be started with: systemctl start casinobot
It can be stopped with: systemctl stop casinobot 

Any time you change one of the config files, stop and then start the bot with those commands.

The account that has the casino's coins is named "treasury". You can deposit to it by sending to this address:
AWfruVA3DgPLk1jDwiw7w5EFgxCYnTTSQ9

To withdraw coins from the treasury, you must use the following command in the console, replacing the address and amount:
aced-cli sendfrom treasury AzX8fA8oiasffakeaddress 0.00001

It is important that you only withdraw by using sendfrom command. Please do not withdraw any other way or it will mess up account balances.

If you want to know the current balance of the treasury account, use the following command:
aced-cli getbalance treasury

There are three config files. There is one file in docs accompanying each config file that explains the settings.

The wallet is located in /root/.acedcore/wallet.dat. 

I strongly recommend enabling auto backups for this server.

And obviously make sure there are enough coins in the treasury account to cover whatever rewards you set.