﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using DCC = CasinoBot.DiscordCommandContext;

namespace CasinoBot.Modules {
    public class Commands : ModuleBase<SocketCommandContext> {
        readonly EventService eventService;

        public Commands(EventService eventService) {
            this.eventService = eventService;
        }


        [Command("spin")] public async Task spin() => 
            eventService.publish(new PlaySlotsRequestedEvent(new DCC(Context), null));
        
        [Command("spin")] public async Task spinRemainder([Remainder] string any) => 
            eventService.publish(new PlaySlotsRequestedEvent(new DCC(Context), null));
        
        [Command("spin")] public async Task spinInt(int betAmount) => 
            eventService.publish(new PlaySlotsRequestedEvent(new DCC(Context), betAmount));
        

        [Command("paytable")] public async Task paytable() {
            eventService.publish(
                new ShowPaytableEvent(new DCC(Context))
            );
        }

        [Command("deposit")] public async Task deposit() {
            eventService.publish(new GetDepositAddressRequestedEvent(new DCC(Context)));
        }

        [Command("balance")] public async Task balance() {
            eventService.publish(new GetBalanceRequestedEvent(Context));
        }

        [Command("withdraw")] public async Task withdraw(string amount = null, string address = null) {
            eventService.publish(new WithdrawRequestedEvent(address, amount, new DCC(Context)));
        }

        [Command("help")] public async Task help() => 
            eventService.publish(new DisplayCommandsRequestedEvent(new DCC(Context)));
        
        [Command("treasuryinfo")] public async Task treasuryinfo() => 
            eventService.publish(new GetTreasuryInfoRequestedEvent(new DCC(Context)));
    }
}