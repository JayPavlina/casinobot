﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace CasinoBot {
    public sealed class CommandHandler {
        readonly SocketClient client;
        readonly BotConfig botConfig;
        readonly SlotsService slotsService;
        readonly EventService eventService;
        readonly CommandService commandService = new CommandService();

        public CommandHandler(SocketClient client, BotConfig botConfig, SlotsService slotsService, EventService eventService) {
            this.client = client;
            this.botConfig = botConfig;
            this.slotsService = slotsService;
            this.eventService = eventService;
        }

        IServiceProvider serviceProvider;

        public async Task installCommandsAsync() {
            client.underlyingClient.MessageReceived += handleCommandAsync;
            serviceProvider = new ServiceCollection()
                .AddSingleton(slotsService)
                .AddSingleton(eventService)
                .BuildServiceProvider();

            await commandService.AddModulesAsync(assembly: Assembly.GetEntryAssembly());
        }

        async Task handleCommandAsync(SocketMessage message) {
            if (!(message is SocketUserMessage userMessage))
                return;

            var argPos = 0;

            if (!(userMessage.HasStringPrefix(botConfig.commandPrefix, ref argPos) 
                || userMessage.HasMentionPrefix(client.underlyingClient.CurrentUser, ref argPos)))
                return;

            var context = new SocketCommandContext(client.underlyingClient, userMessage);

            var result = await commandService.ExecuteAsync(
                context: context,
                argPos: argPos,
                services: serviceProvider
            );

            if (!result.IsSuccess)
                Console.WriteLine(result.ErrorReason);
        }
    }
}