﻿using Discord.WebSocket;

namespace CasinoBot {
    public class SocketClient {
        public readonly DiscordSocketClient underlyingClient;
        
        public SocketClient(BotConfig botConfig) {
            var config = new DiscordSocketConfig() {
                LogLevel = botConfig.logSeverity
            };
            
            underlyingClient = new DiscordSocketClient(config);
        }
    }
}