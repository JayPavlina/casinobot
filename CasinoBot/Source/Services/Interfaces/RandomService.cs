﻿namespace CasinoBot {
    public interface RandomService {
        int next();
        /// Inclusive on both numbers
        int next(int min, int max);

        byte nextByte();
    }
}