﻿using System;

namespace CasinoBot {
    public interface HighLevelCoinService {
        string getWalletAddress(CryptoAccountProvider accountProvider);
        decimal getAccountBalance(CryptoAccountProvider accountProvider);
        bool move(CryptoAccountProvider fromAccountProvider, CryptoAccountProvider toAccountProvider,
            Decimal amount);
        decimal getAddressBalance(CryptoAddressProvider addressProvider, bool? includeWatchonly = null);

        (SendCoinsResult validationResult, string rpcResult) send(CryptoAccountProvider sourceAccount, CryptoAddressProvider destinationAddressProvider,
            decimal amount);
    }
    
    public enum SendCoinsResult {
        invalidSourceAddress, invalidDestinationAddress, success, insufficientBalance
    }

    public interface CryptoAddressProvider {
        string walletAddress {get;}
    }
    
    public interface CryptoAccountProvider {
        string accountName {get;}
    }

    public sealed class DefaultCryptoAccountProvider : CryptoAccountProvider {
        public string accountName {get;}
        public DefaultCryptoAccountProvider(string accountName) => this.accountName = accountName;
    }
    
    public sealed class DefaultCryptoAddressProvider : CryptoAddressProvider {
        public string walletAddress {get;}
        public DefaultCryptoAddressProvider(string walletAddress) => this.walletAddress = walletAddress;
    }
}