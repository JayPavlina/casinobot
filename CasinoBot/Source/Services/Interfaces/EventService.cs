﻿using System;

namespace CasinoBot
{
    public interface IMessagePublisher
    {
        /// <summary>
        /// Send Message to all receiver.
        /// </summary>
        void publish<T>(T message);
    }

    public interface IMessageReceiver
    {
        /// <summary>
        /// Subscribe typed message.
        /// </summary>
        IObservable<T> receive<T>();
    }

    public interface EventService : IMessagePublisher, IMessageReceiver
    {
    }
}