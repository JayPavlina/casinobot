﻿using System.Threading.Tasks;
using Discord;
using JetBrains.Annotations;

namespace CasinoBot {
    public interface SendMessageService {
        Task<IUserMessage> SendMessageAsync(CommandContext context, [NotNull] string text, Embed embed, RequestOptions options = null);
        Task<IUserMessage> SendTextMessageAsync(CommandContext context, string text, RequestOptions options = null);
        Task<IUserMessage> SendEmbeddedMessageAsync(CommandContext context, Embed embed, RequestOptions options = null);
    }
}