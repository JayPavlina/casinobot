﻿namespace CasinoBot {
    public sealed class UserDataService {
        readonly HighLevelCoinService coinService;
        
        public UserDataService(HighLevelCoinService coinService) => this.coinService = coinService;

        public UserData getUserData(ulong id) => new DefaultUserData(id, coinService);
    }
}