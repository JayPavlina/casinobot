﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CasinoBot {
    public partial class SlotsService {
        public const int targetWeight = 256;

        [Conditional("DEBUG")] void assertWeights() {
            var weights = config.reelItems.Select(x => x.weights);
            for (int i = 0; i < config.reelCount; i++) {
                var result = weights.Select(x => x[i]).Aggregate((a, b) => a + b);
                Debug.Assert(result == targetWeight);
            }
        }

        readonly List<ReelItem[]> reelItemsByWeightId = new List<ReelItem[]>();

        void initializeWeights() {
            for (int i = 0; i < config.reelCount; i++)
                reelItemsByWeightId.Add(new ReelItem[targetWeight]);
            int weightId = 0;
            for (var reelId = 0; reelId < config.reelCount; reelId++) {
                weightId = 0;
                foreach (var reelItem in config.reelItems) {
                    for (int i = 0; i < reelItem.weights[reelId]; i++) {
                        reelItemsByWeightId[reelId][weightId] = reelItem;
                        weightId++;
                    }
                }
            }
        }

        public ReelItem getReelItem(int reelId, int weightId) => reelItemsByWeightId[reelId][weightId];
    }
}