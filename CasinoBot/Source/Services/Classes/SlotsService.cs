﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CasinoBot {
    public sealed partial class SlotsService {
        public readonly SlotsConfig config;
        readonly RandomService randomService;
        Dictionary<int, int> betAmountsByIndex;

        public SlotsService(SlotsConfig config, RandomService randomService) {
            this.config = config;
            this.randomService = randomService;
        }

        
        public void initialize() {
            assertWeights();
            betAmountsByIndex = config.allowedBets.ToDictionary(x => x, x => Array.IndexOf(config.allowedBets, x));
            initializeWeights();
            initializeEmojis();
            initializeWinningCombos();
        }

        IEnumerable<WinningCombination> singleWinningCombos;
        IEnumerable<WinningCombination> doubleWinningCombos;
        IEnumerable<WinningCombination> tripleWinningCombos;
        
        void initializeWinningCombos() {
            singleWinningCombos = config.winningCombinations.Where(x => x.count == 1);
            doubleWinningCombos = config.winningCombinations.Where(x => x.count == 2);
            tripleWinningCombos = config.winningCombinations.Where(x => x.count == 3);
        }


        public SlotsResult bet(int betAmount) {
            var weightIds = getSlotWeightIds();
            var indices = (
                getReelItem(0, weightIds.x).index,
                getReelItem(1, weightIds.y).index,
                getReelItem(2, weightIds.z).index
            );
            var result = new SlotsResult(indices, getReward(betAmount, indices), betAmount);
//            var playedSlotsEvent = new PlayedSlotsEvent(result, new SlotsSessionData());
//            betWasPlacedSubject.OnNext(result);
            return result;
        }

        public int getReward(int betAmount, (int x, int y, int z) indices) {
            var x = indices.x;
            var y = indices.y;
            var z = indices.z;
            var betIndex = getBetAmountIndex(betAmount);
            foreach (var combo in tripleWinningCombos) {
                var target = combo.index;
                if (x == target && y == target && z == target)
                    return combo.getReward(betIndex);
            }
            foreach (var combo in doubleWinningCombos) {
                var target = combo.index;
                if ((x == target && y == target) ||
                    (x == target && z == target) ||
                    (y == target && z == target))
                return combo.getReward(betIndex);
            }
            foreach (var combo in singleWinningCombos) {
                var target = combo.index;
                if (x == target || y == target || z == target)
                    return combo.getReward(betIndex);
            }

            return 0;
        }

        public int getBetAmountIndex(int betAmount) => betAmountsByIndex[betAmount];

        public decimal getProcessedBetValue(int betAmount) => betAmount * config.betAndRewardMultiplier;

        public (int x, int y, int z) getSlotWeightIds() =>
            (getRandomSymbolIndex(), getRandomSymbolIndex(), getRandomSymbolIndex());

        int getRandomSymbolIndex() => randomService.nextByte();
    }
}