﻿namespace CasinoBot {
    public sealed class InitializableManager {
        readonly Initializable[] initializables;

        public InitializableManager(Initializable[] initializables) => this.initializables = initializables;

        public void initialize() {
            foreach (var initializable in initializables) {
                initializable.initialize();
            }
        }
    }
}