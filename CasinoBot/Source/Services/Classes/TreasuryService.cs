﻿using System;

namespace CasinoBot {
    public class TreasuryService {
        readonly CasinoTreasury treasury;
        readonly HighLevelCoinService coinService;

        public TreasuryService(CasinoTreasury treasury, HighLevelCoinService coinService) {
            this.treasury = treasury;
            this.coinService = coinService;
        }

        public bool addToTreasury(CryptoAccountProvider accountProvider, Decimal amount) {
            var balance = coinService.getAccountBalance(accountProvider); 
            if (balance < amount)
                return false;
            return coinService.move(accountProvider, treasury, amount);
        }

        public bool removeFromTreasury(Decimal amount, CryptoAccountProvider targetAccountProvider) => 
            coinService.move(treasury, targetAccountProvider, amount);

        public decimal balance => coinService.getAccountBalance(treasury);

        public string walletAddress => treasury.walletAddress;
        public string accountName => treasury.accountName;
    }
}