﻿using System.Threading.Tasks;
using Discord;

namespace CasinoBot {
    public class DiscordSendMessageService : SendMessageService {
        public Task<IUserMessage> SendMessageAsync(CommandContext context, string text, Embed embed, RequestOptions options = null) => 
            context.native.Channel.SendMessageAsync(text: text, isTTS: false, embed: embed);

        public Task<IUserMessage>
            SendTextMessageAsync(CommandContext context, string text, RequestOptions options = null) =>
            context.native.Channel.SendMessageAsync(text);

        public Task<IUserMessage> SendEmbeddedMessageAsync(CommandContext context, Embed embed = null,
            RequestOptions options = null) =>
            context.native.Channel.SendMessageAsync(text: "", isTTS: false, embed: embed);
        
    }
}