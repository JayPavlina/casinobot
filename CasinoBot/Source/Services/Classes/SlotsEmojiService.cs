﻿using System.Collections.Generic;
using System.Linq;

namespace CasinoBot {
    public partial class SlotsService {
        Dictionary<int, string> emojisByIndex;

        void initializeEmojis() {
            emojisByIndex = config.reelItems.ToDictionary(x => x.index, x => x.emoji);
        }
        
        public string getEmoji(int index) => $"{emojisByIndex[index]}";
    }
}