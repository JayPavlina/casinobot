﻿using System;

namespace CasinoBot {
    public sealed partial class SlotsService {
        string[][] table;
        int paytableWidth = 4;
        int paytableHeight;
        const int spacing = 10;

        public string generatePaytable() {
            if (table == null)
                createTable();
            var output = "";
            for (var y = 0; y < paytableHeight; y++) {
                output += String.Format("{0,10}{1,10}{2,10}{3,10}", getXY(0, y), getXY(1, y), getXY(2, y), getXY(3, y));;
                output += "\n";
            }

            return output;
        }

        string[][] createTable() {
            paytableHeight = config.winningCombinations.Length + 1;
            table = new string[paytableHeight][];
            for (var j = 0; j < paytableHeight; j++)
                table[j] = new string[paytableWidth];

            var i = 1;
            setXY(0, 0, "Target");
            foreach (var betAmount in config.allowedBets)
                setXY(i++, 0, $"{betAmount} Coins");

            for (var comboIndex = 0; comboIndex < config.winningCombinations.Length; comboIndex++) {
                var combo = config.winningCombinations[comboIndex];
                var y = comboIndex + 1;
                setXY(0, y, $"{getEmoji(combo.index)} x {combo.count}");
                for (var betIndex = 0; betIndex < config.allowedBets.Length; betIndex++)
                    setXY(betIndex + 1, y, combo.rewards[betIndex].ToString());
            }

            return table;
        }

        string getXY(int x, int y) => table[y][x];
        string setXY(int x, int y, string value) => table[y][x] = value;
    }
}