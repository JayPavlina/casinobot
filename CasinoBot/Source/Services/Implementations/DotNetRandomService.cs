﻿using System;

namespace CasinoBot {
    public sealed class DotNetRandomService : RandomService {
        readonly Random random = new Random();
        public int next() => random.Next();
        public int next(int min, int max) => random.Next(min, max + 1);
        public byte nextByte() => throw new NotImplementedException();
    }
}