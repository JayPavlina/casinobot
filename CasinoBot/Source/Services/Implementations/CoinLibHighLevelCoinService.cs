﻿using System;
using System.Linq;
using BitcoinLib.RPC.Connector;
using BitcoinLib.RPC.Specifications;
using BitcoinLib.Services.Coins.Cryptocoin;

namespace CasinoBot {
    public sealed class CoinLibHighLevelCoinService : HighLevelCoinService {
        readonly ICryptocoinService coinService;
        readonly RpcConfig config;
        readonly IRpcConnector rpcConnector;

        public CoinLibHighLevelCoinService(CryptoCoinServiceAdapter coinServiceAdapter, RpcConfig config) {
            this.config = config;
            coinService = coinServiceAdapter.service;
            rpcConnector = new RpcConnector(coinService);
        }

        public string sendFrom_custom(string fromAccount, string toBitcoinAddress, Decimal amount, int minConf,
            string comment, string commentTo) => rpcConnector.MakeRequest<string>(RpcMethods.sendfrom,
            (object)fromAccount, (object)toBitcoinAddress, (object)amount, (object)minConf, (object)false,
            (object)comment, (object)commentTo);

        bool move_custom(string fromAccount, string toAccount, Decimal amount) =>
            rpcConnector.MakeRequest<bool>(RpcMethods.move, (object)fromAccount, (object)toAccount, (object)amount);

        public bool move(CryptoAccountProvider fromAccountProvider, CryptoAccountProvider toAccountProvider,
            Decimal amount) =>
            coinService.Move(fromAccountProvider.accountName, toAccountProvider.accountName, amount,
                config.minimumConfirmationsForMove);

        public string getWalletAddress(CryptoAccountProvider accountProvider) =>
            coinService.GetAccountAddress(accountProvider.accountName);

        public decimal getAddressBalance(CryptoAddressProvider addressProvider, bool? includeWatchonly = null) =>
            coinService.GetAddressBalance(addressProvider.walletAddress, config.minimumConfirmationsForGetBalance);

        public decimal getAccountBalance(CryptoAccountProvider accountProvider) =>
            coinService.GetBalance(accountProvider.accountName, config.minimumConfirmationsForGetBalance);

        public (SendCoinsResult validationResult, string rpcResult) send(CryptoAccountProvider sourceAccount,
            CryptoAddressProvider destinationAddressProvider, decimal amount) {
            if (!coinService.GetAddressesByAccount(sourceAccount.accountName)?.Any() ?? true)
                return (SendCoinsResult.invalidSourceAddress, null);
            if (!coinService.ValidateAddress(destinationAddressProvider.walletAddress).IsValid)
                return (SendCoinsResult.invalidDestinationAddress, null);
            if (getAccountBalance(sourceAccount) < amount)
                return (SendCoinsResult.insufficientBalance, null);
            string rpcResult = null;
            try {
                rpcResult = sendFrom_custom(sourceAccount.accountName, destinationAddressProvider.walletAddress, amount,
                    config.minimumConfirmationsForWithdraw, "", "");
            }
            catch (Exception e) {
                Console.WriteLine(e);
                Console.WriteLine(rpcResult);
                throw;
            }

            return (SendCoinsResult.success, rpcResult);
        }
    }
}