﻿using System;
using System.Security.Cryptography;

namespace CasinoBot {
    public class CryptoRandomService : RandomService {
        readonly RNGCryptoServiceProvider rngProvider = new RNGCryptoServiceProvider();
        readonly byte[] buffer = new byte[1];

        public int next() => throw new NotImplementedException();

        public int next(int min, int max) => throw new NotImplementedException();

        public byte nextByte() {
            rngProvider.GetBytes(buffer);
            return buffer[0];
        }
    }
}