﻿using Newtonsoft.Json;

namespace CasinoBot {
    public interface SlotsConfig {
        int reelCount {get;}
        int[] allowedBets {get;}
        WinningCombination[] winningCombinations {get;}
        ReelItem[] reelItems {get;}
        string loseMessage {get;}
        string invalidBetAmountMessage {get;}
        string invalidFormatMessage {get;}
        decimal betAndRewardMultiplier {get;}
    }
    
    public sealed class DefaultSlotsConfig : SlotsConfig {
        public int reelCount {get; set;} //= 3;
        public int[] allowedBets {get; set;} //= new[] {1, 2, 3};
        public ReelItem[] reelItems {get; set;} //= new[] {new ReelItem("watermelon", new[] {1, 2, 3}, 10)};
        public WinningCombination[] winningCombinations {get; set;}
        public string loseMessage {get; set;} //= "Better luck next time!";
        public string invalidBetAmountMessage {get; set;} //= "Only bets of 1, 2, or 3 AceD will be accepted";

        public string invalidFormatMessage {get; set;} //= "Slots command must be in the format `!slots betAmount`, for example: `!slots 1` to bet 1 AceD";

        public decimal betAndRewardMultiplier {get; set;} //= (decimal).00001;
    }

    [JsonObject]
    public sealed class WinningCombination {
        public int index {get;}
        public int count {get;}
        public int[] rewards {get;}
        
        public WinningCombination(int index, int count, int[] rewards) {
            this.index = index;
            this.count = count;
            this.rewards = rewards;
        }

        public int getReward(int betAmountIndex) => rewards[betAmountIndex];
    }

    [JsonObject]
    public sealed class ReelItem {
        public string emoji {get;}
        public int[] weights {get;}
        public int index {get;}

        public ReelItem(string emoji, int[] weights, int index) {
            this.emoji = emoji;
            this.weights = weights;
            this.index = index;
        }
    }
}