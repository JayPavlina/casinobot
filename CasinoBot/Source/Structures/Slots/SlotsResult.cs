﻿namespace CasinoBot {
    public readonly struct SlotsResult {
        public readonly int betAmount;
        public readonly (int x, int y, int z) indices;
        public readonly int reward;

        public SlotsResult((int x, int y, int z) indices, int reward, int betAmount) {
            this.indices = indices;
            this.betAmount = betAmount;
            this.reward = reward;
        }
    }
}