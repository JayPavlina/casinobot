﻿using BitcoinLib.Services.Coins.Cryptocoin;

namespace CasinoBot {
    public sealed class CryptoCoinServiceAdapter {
        public readonly ICryptocoinService service;

        public CryptoCoinServiceAdapter(RpcConfig config, ICryptocoinService service = null) {
            this.service = service ?? new CryptocoinService(
                    daemonUrl: config.daemonUrl,
                    rpcUsername: config.rpcUsername,
                    rpcPassword: config.rpcPassword,
                    walletPassword: "",
                    rpcRequestTimeoutInSeconds: config.rpcRequestTimeoutInSeconds
            );
        }
    }
}