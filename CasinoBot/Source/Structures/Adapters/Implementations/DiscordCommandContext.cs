﻿using Discord.Commands;

namespace CasinoBot {
    public sealed class DiscordCommandContext : CommandContext {
        public ICommandContext native {get;}

        public DiscordCommandContext(ICommandContext native) => this.native = native;
    }
}