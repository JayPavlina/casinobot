﻿using Discord;

namespace CasinoBot {
    public interface BotConfig {
        string token {get;}
        LogSeverity logSeverity {get;}
        string commandPrefix {get;}
        CommandInfo[] commands {get;}
        string invalidWithdrawMessage {get;}
        bool allowTreasuryinfoCommand {get;}
    }

    public sealed class DefaultBotConfig : BotConfig {
        public string token {get; set;}
        public LogSeverity logSeverity {get; set;} //= LogSeverity.Verbose;
        public string commandPrefix {get; set;} //= "!";
        public CommandInfo[] commands {get; set;}
        public string invalidWithdrawMessage {get; set;} //= "Invalid withdraw";
        public bool allowTreasuryinfoCommand {get; set;}
    }

    public class CommandInfo {
        public string name {get;}
        public string description {get;}
        public string example {get;}
        
        public CommandInfo(string name, string description, string example) {
            this.name = name;
            this.description = description;
            this.example = example;
        }
    }
}