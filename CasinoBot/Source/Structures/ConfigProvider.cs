﻿namespace CasinoBot {
    public sealed class ConfigProvider {
        public readonly RpcConfig rpcConfig;
        public readonly SlotsConfig slotsConfig;
        
        public ConfigProvider(RpcConfig rpcConfig, SlotsConfig slotsConfig) {
            this.rpcConfig = rpcConfig;
            this.slotsConfig = slotsConfig;
        }
    }
}