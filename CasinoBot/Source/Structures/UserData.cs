﻿namespace CasinoBot {
    public interface UserData : CryptoAccountProvider, CryptoAddressProvider {
        ulong id {get;}
    }

    public sealed class DefaultUserData : UserData {
        public ulong id {get;}
        public string accountName => id.ToString();
        public string walletAddress {get;}

        public DefaultUserData(ulong id, string walletAddress) {
            this.id = id;
            this.walletAddress = walletAddress;
        }
        
        public DefaultUserData(ulong id, HighLevelCoinService coinService) {
            this.id = id;
            this.walletAddress = coinService.getWalletAddress(this);
        }
    }
}