﻿namespace CasinoBot {
    public class CasinoTreasury : CryptoAccountProvider, CryptoAddressProvider {
        public string accountName {get;} = "treasury";
        public string walletAddress {get;}
        
        public CasinoTreasury(HighLevelCoinService coinService) => walletAddress = coinService.getWalletAddress(this);
    }
}