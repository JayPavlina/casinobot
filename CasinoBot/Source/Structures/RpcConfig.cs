﻿namespace CasinoBot {
    public interface RpcConfig {
        string daemonUrl {get;}
        string rpcUsername {get;}
        string rpcPassword {get;}
        string walletPassword {get;}
        int minimumConfirmationsForGetBalance {get;}
        int minimumConfirmationsForWithdraw {get;}
        int minimumConfirmationsForMove {get;}
        short rpcRequestTimeoutInSeconds {get;}
    }
    
    public sealed class DefaultRpcConfig : RpcConfig {
        public string daemonUrl {get; set;}
        public string rpcUsername {get; set;}
        public string rpcPassword {get; set;}
        public string walletPassword {get; set;}
        public int minimumConfirmationsForGetBalance {get; set;}
        public int minimumConfirmationsForWithdraw {get; set;}
        public int minimumConfirmationsForMove {get; set;}
        public short rpcRequestTimeoutInSeconds {get; set;}
    }
}