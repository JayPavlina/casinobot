﻿using Discord.WebSocket;

namespace CasinoBot {
    public readonly struct RequestPostMessageEvent {
        public readonly string message;
        public readonly ISocketMessageChannel channel;
        
        public RequestPostMessageEvent(string message, ISocketMessageChannel channel) {
            this.message = message;
            this.channel = channel;
        }
    }
}