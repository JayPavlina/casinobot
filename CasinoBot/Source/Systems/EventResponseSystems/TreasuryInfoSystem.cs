using Discord;

namespace CasinoBot {
    public class TreasuryInfoSystem : EventResponseSystem<GetTreasuryInfoRequestedEvent> {
        readonly TreasuryService treasuryService;
        readonly BotConfig botConfig;

        public TreasuryInfoSystem(EventService eventService, TreasuryService treasuryService, BotConfig botConfig) : base(eventService) {
            this.treasuryService = treasuryService;
            this.botConfig = botConfig;
        }
        
        protected override void onEventReceived(GetTreasuryInfoRequestedEvent x) {
            if (!botConfig.allowTreasuryinfoCommand)
                return;
            var embedBuilder = new EmbedBuilder()
                .AddField("Account Name", treasuryService.accountName, false)
                .AddField("Address", treasuryService.walletAddress, false)
                .AddField("Balance", treasuryService.balance, false);
            x.context.native.Channel.SendMessageAsync($"", false, embedBuilder.Build());
        }
    }

    public sealed class GetTreasuryInfoRequestedEvent {
        public readonly CommandContext context;
        
        public GetTreasuryInfoRequestedEvent(CommandContext context) => this.context = context;
    }
}