﻿using Discord.Commands;

namespace CasinoBot {
    public sealed class WithdrawSystem : EventResponseSystem<WithdrawRequestedEvent> {
        public const string allCoins = "all";
        
        readonly HighLevelCoinService coinService;
        readonly UserDataService userDataService;
        readonly BotConfig botConfig;

        public WithdrawSystem(EventService eventService, HighLevelCoinService coinService, UserDataService userDataService, BotConfig botConfig) : base(eventService) {
            this.coinService = coinService;
            this.userDataService = userDataService;
            this.botConfig = botConfig;
        }

//        public override void initialize() {
//            base.initialize();
//            coinService.unlockWallet();
//        }

        protected override void onEventReceived(WithdrawRequestedEvent x) {
            var context = x.commandContext.native;
            if (!validateArgs(x.address, x.amount)) {
                sendMessage(context, botConfig.invalidWithdrawMessage);
                return;
            }

//            decimal amount;
//            if (x.amount == allCoins)
//                throw new NotImplementedException();
//            else
            var amount = decimal.Parse(x.amount);
            var user = userDataService.getUserData(context.User.Id);
            var result = coinService.send(user, new DefaultCryptoAddressProvider(x.address), amount);
            sendMessage(context, getWithdrawMessage(result, x.commandContext));
        }

        string getWithdrawMessage((SendCoinsResult validationResult, string rpcResult) result, CommandContext context) {
            var userName = context.native.User.Username;
            switch (result.validationResult) {
                case SendCoinsResult.invalidSourceAddress: 
                    return $"Withdraw failed for **{userName}** with unexpected error. Please contact an administrator.";
                case SendCoinsResult.invalidDestinationAddress:
                    return $"Withdraw failed for **{userName}**. The destination address is invalid";
                case SendCoinsResult.insufficientBalance: 
                    return $"Withdraw failed for **{userName}**. Insufficient balance";
                case SendCoinsResult.success: 
                    return $"Withdraw succeeded with transaction id: {result.rpcResult}";
            }

            return null;
        }

        public bool validateArgs(string address, string amount) {
            if (string.IsNullOrEmpty(address) || string.IsNullOrEmpty(amount))
                return false;
            if (amount == allCoins)
                return true;
            if (decimal.TryParse(amount, out var coinCount))
                return true;
            return false;
        }

        void sendMessage(ICommandContext context, string message) => context.Channel.SendMessageAsync(message);
    }

//    enum WithdrawResult {
//        invalidFormat,
//    }

    public sealed class WithdrawRequestedEvent {
        public readonly string address;
        public readonly string amount;
        public readonly CommandContext commandContext;

        public WithdrawRequestedEvent(string address, string amount, CommandContext commandContext) {
            this.address = address;
            this.amount = amount;
            this.commandContext = commandContext;
        }
    }
}