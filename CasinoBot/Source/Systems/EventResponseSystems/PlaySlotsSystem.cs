﻿using System;
using System.Linq;
using Discord;

namespace CasinoBot {
    public sealed class PlaySlotsSystem : EventResponseSystem<PlaySlotsRequestedEvent> {
        readonly SlotsService slotsService;
        readonly SlotsConfig slotsConfig;
        readonly SendMessageService messageService;
        readonly TreasuryService treasuryService;
        readonly UserDataService userDataService;
        readonly RpcConfig rpcConfig;

        public PlaySlotsSystem(EventService eventService, SlotsService slotsService, SendMessageService messageService,
            ConfigProvider configProvider, TreasuryService treasuryService, UserDataService userDataService) : base(
            eventService) {
            this.slotsService = slotsService;
            slotsConfig = slotsService.config;
            rpcConfig = configProvider.rpcConfig;
            this.messageService = messageService;
            this.treasuryService = treasuryService;
            this.userDataService = userDataService;
            eventService.receive<ShowPaytableEvent>().Subscribe(x => {
                x.context.native.Channel.SendMessageAsync(slotsService.generatePaytable());
            });
        }

        protected override void onEventReceived(PlaySlotsRequestedEvent x) {
            var validationResult = validate(x.betAmount);
            if (validationResult != ValidationResult.valid) {
                messageService.SendMessageAsync(x.context, getMessage(validationResult, x.context, null).text, null);
                return;
            }

            var actualBetValue = slotsService.getProcessedBetValue(x.betAmount.Value);
            var userData = userDataService.getUserData(x.context.native.User.Id);
            if (!treasuryService.addToTreasury(userData, actualBetValue)) {
                messageService.SendMessageAsync(x.context,
                    "You do not have enough coins to bet that amount.", null);
                return;
            }

            var slotsResult = slotsService.bet(x.betAmount.Value);
            var message = getMessage(validationResult, x.context, slotsResult);
            messageService.SendMessageAsync(x.context, message.text, message.embed);
            if (slotsResult.reward > 0)
                treasuryService.removeFromTreasury(slotsResult.reward * slotsConfig.betAndRewardMultiplier, userData);
        }

        (string text, Embed embed) getMessage(ValidationResult validationResult, CommandContext context,
            SlotsResult? slotsResult) {
            switch (validationResult) {
                case ValidationResult.invalidBetAmount:
                    return (slotsConfig.invalidBetAmountMessage, null);
                case ValidationResult.wrongFormat:
                    return (slotsConfig.invalidFormatMessage, null);
//                case ValidationResult.insufficientBalance:
//                    return (, null);
                case ValidationResult.valid: {
                    var betAmount = slotsResult.Value.betAmount;
                    var indices = slotsResult.Value.indices;

                    var embedBuilder = new EmbedBuilder().WithDescription(
                        $"**{context.native.User.Username}** placed a bet of {betAmount} coin{getSIfShouldBePlural(betAmount)}" +
                        $"\n\n{slotsService.getEmoji(indices.x)} {slotsService.getEmoji(indices.y)} {slotsService.getEmoji(indices.z)}" +
                        $"\n\n{getResultMessage()}"
                    );
                    return ("", embedBuilder.Build());
                }
            }

            throw new Exception();

            string getResultMessage() {
                var win = slotsResult.Value.reward > 0;
                return win
                    ? $"You won {slotsResult.Value.reward} coins!"
                    : slotsConfig.loseMessage;
            }
        }

        public ValidationResult validate(int? betAmount) {
            if (betAmount == null)
                return ValidationResult.wrongFormat;
            if (!slotsConfig.allowedBets.Contains(betAmount.Value))
                return ValidationResult.invalidBetAmount;
            return ValidationResult.valid;
        }

        static string getSIfShouldBePlural(int count) => count != 1 ? "s" : "";

        public enum ValidationResult {
            wrongFormat,
            invalidBetAmount,
            valid
        }
    }

    public readonly struct PlaySlotsRequestedEvent {
        public readonly CommandContext context;
        public readonly int? betAmount;

        public PlaySlotsRequestedEvent(CommandContext context, int? betAmount) {
            this.context = context;
            this.betAmount = betAmount;
        }
    }

    public readonly struct ShowPaytableEvent {
        public readonly CommandContext context;
        public ShowPaytableEvent(CommandContext context) => this.context = context;
    }

//    public readonly struct PlayedSlotsEvent {
//        public readonly SlotsResult result;
//        public readonly SlotsSessionData sessionData;
//
//        public PlayedSlotsEvent(SlotsResult result, SlotsSessionData sessionData) {
//            this.result = result;
//            this.sessionData = sessionData;
//        }
//    }
}