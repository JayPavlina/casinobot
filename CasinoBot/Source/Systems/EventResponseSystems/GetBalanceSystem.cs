﻿using System;
using Discord.Commands;

namespace CasinoBot {
    public sealed class GetBalanceSystem : EventResponseSystem<GetBalanceRequestedEvent> {
        readonly HighLevelCoinService coinService;
        readonly UserDataService userDataService;

        public GetBalanceSystem(EventService eventService, HighLevelCoinService coinService, UserDataService userDataService) : base(eventService) {
            this.coinService = coinService;
            this.userDataService = userDataService;
        }

        protected override void onEventReceived(GetBalanceRequestedEvent x) {
            var userData = userDataService.getUserData(x.context.User.Id);
            var balance = coinService.getAccountBalance(userData);
            eventService.publish(new BalanceWasRetreivedEvent(balance, x.context));
        }
    }
    
    public sealed class GetBalanceRequestedEvent {
        public readonly ICommandContext context;
        public GetBalanceRequestedEvent(ICommandContext context) => this.context = context;
    }
    
    public sealed class BalanceWasRetreivedEvent {
        public readonly Decimal balance;
        public readonly ICommandContext context;
        
        public BalanceWasRetreivedEvent(decimal balance, ICommandContext context) {
            this.balance = balance;
            this.context = context;
        }
    }
}