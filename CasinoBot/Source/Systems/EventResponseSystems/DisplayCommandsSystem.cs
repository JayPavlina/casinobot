﻿using System;
using System.Linq;
using Discord;

namespace CasinoBot {
    public sealed class DisplayCommandsSystem : EventResponseSystem<DisplayCommandsRequestedEvent> {
        readonly BotConfig botConfig;
        readonly string description;
        
        public DisplayCommandsSystem(EventService eventService, BotConfig botConfig) : base(eventService) {
            this.botConfig = botConfig;
            description = generateDescription();
        }
        
        protected override void onEventReceived(DisplayCommandsRequestedEvent x) {
            var embedBuilder = new EmbedBuilder {Description = description};
            x.context.native.Channel.SendMessageAsync("", false, embedBuilder.Build());
        }

        string generateDescription() {
            var output = "";
            var commandInfos = botConfig.commands.OrderBy(y => y.name);
            var lastName = commandInfos.Last().name;
            foreach (var commandInfo in commandInfos) {
                output += $"**{botConfig.commandPrefix}{commandInfo.name}:** {commandInfo.description}";
                if (!string.IsNullOrEmpty(commandInfo.example)) {
                    output += "\n";
                    output += $"*Example:* `{botConfig.commandPrefix}{commandInfo.example}`";
                }
                if (commandInfo.name != lastName)
                    output += "\n";
            }
            return output;
        }
    }

    public readonly struct DisplayCommandsRequestedEvent {
        public readonly CommandContext context;
        public DisplayCommandsRequestedEvent(CommandContext context) => this.context = context;
    }
}