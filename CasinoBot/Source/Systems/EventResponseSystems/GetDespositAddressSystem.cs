﻿namespace CasinoBot {
    public sealed class GetDespositAddressSystem : EventResponseSystem<GetDepositAddressRequestedEvent> {
        readonly HighLevelCoinService coinService;
        readonly UserDataService userDataService;

        public GetDespositAddressSystem(EventService eventService, HighLevelCoinService lowLevelCoinService, UserDataService userDataService) : base(eventService) {
            this.coinService = lowLevelCoinService;
            this.userDataService = userDataService;
        }

        protected override void onEventReceived(GetDepositAddressRequestedEvent x) {
            var userData = userDataService.getUserData(x.context.native.User.Id);
            var address = coinService.getWalletAddress(userData);
            eventService.publish(new DepositAddressWasRetreivedEvent(address, x.context));
        }
    }
    
    public sealed class GetDepositAddressRequestedEvent {
        public readonly CommandContext context;
        public GetDepositAddressRequestedEvent(CommandContext context) => this.context = context;
    }
    
    public sealed class DepositAddressWasRetreivedEvent {
        public readonly string address;
        public readonly CommandContext context;
        
        public DepositAddressWasRetreivedEvent(string address, CommandContext context) {
            this.address = address;
            this.context = context;
        }
    }
}