﻿using System;

namespace CasinoBot {
    public abstract class EventResponseSystem<T> : Initializable {
        protected readonly EventService eventService;
        protected EventResponseSystem(EventService eventService) => this.eventService = eventService;

        public virtual void initialize() => eventService.receive<T>().Subscribe(onEventReceived);

        protected abstract void onEventReceived(T x);
    }
}