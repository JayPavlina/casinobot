﻿using System;
using System.Threading.Tasks;
using Discord;

namespace CasinoBot {
    public sealed class InitializeBotSystem {
        readonly BotConfig botConfig;
        readonly SocketClient client;
        readonly CommandHandler commandHandler;

        public InitializeBotSystem(BotConfig botConfig, SocketClient client, CommandHandler commandHandler) {
            this.botConfig = botConfig;
            this.client = client;
            this.commandHandler = commandHandler;
        }

        public async Task initialize() {
            client.underlyingClient.Log += x => {
                Console.WriteLine(x.ToString());
                return Task.CompletedTask;
            };

            await client.underlyingClient.LoginAsync(TokenType.Bot, botConfig.token);
            await client.underlyingClient.StartAsync();
            
            await commandHandler.installCommandsAsync();

            await Task.Delay(-1);
        }
    }
}