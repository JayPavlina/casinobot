﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace CasinoBot {
    public sealed class PopulateConfigSystem {
        const string rootPath = "root/casinobot/Assets/";
        const string
            botConfigPath = rootPath + "botConfig.json",
            rpcConfigPath = rootPath + "rpcConfig.json",
            slotsConfigPath = rootPath + "slotsConfig.json";

        readonly Dictionary<string, object> configsByPath;

        public PopulateConfigSystem(BotConfig botConfig, RpcConfig rpcConfig, SlotsConfig slotsConfig) =>
            configsByPath = new Dictionary<string, object>() {
                [botConfigPath] = botConfig,
                [rpcConfigPath] = rpcConfig,
                [slotsConfigPath] = slotsConfig,
            };

        public void populate() {
            foreach (var pathConfigPair in configsByPath) {
                var path = getFullPath(pathConfigPair.Key);
                var json = File.ReadAllText(path);
                JsonConvert.PopulateObject(json, pathConfigPair.Value);
            }

            var slotsConfig = (DefaultSlotsConfig)configsByPath[slotsConfigPath];
            slotsConfig.reelItems = slotsConfig.reelItems.OrderBy(x => x.index).ToArray();
        }

        static string getFullPath(string localPath) => Path.Combine(Directory.GetCurrentDirectory(), localPath);
    }
}