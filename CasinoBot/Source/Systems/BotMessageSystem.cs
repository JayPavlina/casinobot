﻿using System;

namespace CasinoBot {
    public sealed class BotMessageSystem : Initializable {
        readonly EventService eventService;

        public BotMessageSystem(EventService eventService) => this.eventService = eventService;

        public void initialize() {
            eventService.receive<DepositAddressWasRetreivedEvent>().Subscribe(x => {
                x.context.native.Channel.SendMessageAsync($"**{x.context.native.User.Username}**'s address is {x.address}");
            });
            
            eventService.receive<BalanceWasRetreivedEvent>().Subscribe(x => {
                x.context.Channel.SendMessageAsync($"**{x.context.User.Username}** has a balance of {x.balance}");
            });

            eventService.receive<RequestPostMessageEvent>().Subscribe(x => {
                x.channel.SendMessageAsync(text: x.message);
            });
        }
    }
}