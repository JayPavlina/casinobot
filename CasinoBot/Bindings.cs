﻿using System.Reactive.Concurrency;
using Ninject.Modules;

namespace CasinoBot {
    public sealed class Bindings : NinjectModule {
        public override void Load() {
            // handlers
            Bind<CommandHandler>().ToSelf().InSingletonScope();
            
            // configs
            Bind<BotConfig>().To<DefaultBotConfig>().InSingletonScope();
            Bind<SlotsConfig>().To<DefaultSlotsConfig>().InSingletonScope();
            Bind<RpcConfig>().To<DefaultRpcConfig>().InSingletonScope();
            
            // structures
            Bind<SocketClient>().ToSelf().InSingletonScope();
            Bind<ConfigProvider>().ToSelf().InSingletonScope();
            Bind<CasinoTreasury>().ToSelf().InSingletonScope();
            
            // other
            Bind<IScheduler>().ToConstant(Scheduler.Default).InSingletonScope();
            Bind<InitializableManager>().ToSelf().InSingletonScope();
            
            // systems
            Bind<PopulateConfigSystem>().ToSelf().InSingletonScope();
            Bind<InitializeBotSystem>().ToSelf().InSingletonScope();
            Bind<Initializable>().To<BotMessageSystem>().InSingletonScope();
            Bind<Initializable>().To<PlaySlotsSystem>().InSingletonScope();
            Bind<Initializable>().To<GetBalanceSystem>().InSingletonScope();
            Bind<Initializable>().To<GetDespositAddressSystem>().InSingletonScope();
            Bind<Initializable>().To<WithdrawSystem>().InSingletonScope();
            Bind<Initializable>().To<DisplayCommandsSystem>().InSingletonScope();
            Bind<Initializable>().To<TreasuryInfoSystem>().InSingletonScope();
            
            // services
            Bind<RandomService>().To<CryptoRandomService>().InSingletonScope();
            Bind<SlotsService>().ToSelf().InSingletonScope();
            Bind<TreasuryService>().ToSelf().InSingletonScope();
            Bind<EventService>().To<ReactiveEventService>().InSingletonScope();
            Bind<SendMessageService>().To<DiscordSendMessageService>().InSingletonScope();
            Bind<HighLevelCoinService>().To<CoinLibHighLevelCoinService>().InSingletonScope();
            Bind<CryptoCoinServiceAdapter>().ToSelf().InSingletonScope().WithConstructorArgument("service", (object)null);
        }
    }
}